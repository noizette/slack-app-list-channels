## Setup
- Clone this git
- Run `composer update`
- Point your web server to `www-public/`
- Create a Slack app for you workspace (you can use the below manifest)
- Copy `config.dist.php` to `config.local.php` and fill with your own values
- Copy `categories.dist.php` to `categories.local.php` and fill with your own values
- Create a cron job to exec `cron.php`

## App manifest sample
```
display_information:
  name: Channel list
  description: List channels in defined categories
  background_color: "#7a167a"
  long_description: List existing channels (non-archived) and categorize them with workspace defined rules. Also alert in a specified chan when a channel is created/archived/unarchived/renamed/deleted. Send message "/help" to get usage.
features:
  app_home:
    home_tab_enabled: true
    messages_tab_enabled: false
    messages_tab_read_only_enabled: false
  bot_user:
    display_name: Channel list
    always_online: true
  slash_commands:
    - command: /list
      url: https://example.org/chan-list/
      description: List rules
      should_escape: false
    - command: /add-rule
      url: https://example.org/chan-list/
      description: Add/edit rule
      usage_hint: <rule> <category id>
      should_escape: false
    - command: /help
      url: https://example.org/chan-list/
      description: Message d'aide
      should_escape: false
    - command: /rm-rule
      url: https://example.org/chan-list/
      description: Supprime une règle
      usage_hint: <règle>
      should_escape: false
    - command: /add-cat
      url: https://example.org/chan-list/
      description: Ajoute une catégorie
      usage_hint: <catégorie-id> <titre>
      should_escape: false
    - command: /rm-cat
      url: https://example.org/chan-list/
      description: Supprime une catégorie
      usage_hint: <catégorie-id>
      should_escape: false
oauth_config:
  scopes:
    bot:
      - channels:history
      - channels:join
      - channels:read
      - commands
      - im:history
      - incoming-webhook
      - chat:write
settings:
  event_subscriptions:
    request_url: https://example.org/chan-list/
    bot_events:
      - app_home_opened
      - channel_created
      - channel_deleted
      - channel_rename
  interactivity:
    is_enabled: true
    request_url: https://example.org/chan-list/
  org_deploy_enabled: false
  socket_mode_enabled: false
  token_rotation_enabled: false
```

You can add `- message.im` to event subscriptions, but it seems to make Slack sends a lot of request (way more than needed)?
