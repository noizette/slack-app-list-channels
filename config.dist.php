<?php
// Copy this file as config.local.php and set your own values

define( 'ANNOUNCE_CHAN', 'https://hooks.slack.com/services/****' );
define( 'ANNOUNCE_CHAN_ID', 'BLBLBLBL' );
define( 'LOG_FILE', 'php://stderr' );
define( 'CACHE_FILE', '/somewhere/cache.json' );
define( 'CACHE_DELAY', 86400 );
define( 'JOINED_CHAN', '✅' );
define( 'NOT_JOINED_CHAN', '' );

define( 'MESSAGES', [
	'header'	=> sprintf( "_Les channels les plus récents sont affichés en premier. Ceux que tu as rejoint son marqués par %s._\n\n"
		."Channel\t\t\t\t\t\tDescription\t\t\t\t\t\t\t\t\t\tNombre de membres\tDernière activité", JOINED_CHAN ),
	'caching'	=> '🙁 Les données sont pas en cache, ça mouline, reviens dans 2min genre.',
]);

putenv( 'SLACK_SIGNING_KEY=***' );
putenv( 'SLACK_BOT_TOKEN=xoxb-***' );