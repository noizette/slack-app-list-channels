<?php

use SlackPhp\Framework\Clients\SimpleApiClient;

const CAT_FILE = __DIR__.'/categories.json';

class WebHook {
	// Trick to use sendJsonRequest, as it's a private method
	use SlackPhp\Framework\Clients\SendsHttpRequests;

	public function sendMessage( string $url, string $message) {
		$this->sendJsonRequest( 'POST', $url, [ 'text' => $message] );
	}
}

function parallel_cache(): void {
	$cmd = 'php '.__DIR__.'/cron.php';
	exec(sprintf("%s > %s 2>&1 & echo $! > %s", $cmd, '/dev/null', __DIR__.'/cron.pid'));
}

function get_categories(): array
{
	$data = json_decode( @file_get_contents( CAT_FILE ), true );
	if ( null === $data)
	{
		$data = [ 'titles' => [ 'other' => 'Autres' ], 'rules' => [] ];
	}
	return [ $data['titles'], $data['rules'] ];
}

function add_rule( string $rule, string $cat)
{
	[ $titles, $rules ] = get_categories();
	$rules[$rule] = $cat;
	$data = json_encode( [ 'titles' => $titles, 'rules' => $rules ]);
	return file_put_contents( CAT_FILE, $data);
}

function rm_rule( string $rule )
{
	[ $titles, $rules ] = get_categories();
	unset( $rules[$rule] );
	$data = json_encode( [ 'titles' => $titles, 'rules' => $rules ]);
	return file_put_contents( CAT_FILE, $data);
}

function add_cat( string $cat, string $title)
{
	[ $titles, $rules ] = get_categories();
	$titles[$cat] = $title;
	$data = json_encode( [ 'titles' => $titles, 'rules' => $rules ]);
	return file_put_contents( CAT_FILE, $data);
}

function rm_cat( string $cat )
{
	[ $titles, $rules ] = get_categories();
	unset( $titles[$cat] );
	$data = json_encode( [ 'titles' => $titles, 'rules' => $rules ]);
	return file_put_contents( CAT_FILE, $data);
}

function get_help(): string
{
	$cmds = [
		'/help' => 'Affiche ce message d\'aide',
		'/list' => 'Liste les catégories et règles',
		'/add-cat' => 'Ajoute une catégorie',
		'/rm-cat' => 'Supprime une catégorie',
		'/add-rule' => 'Ajoute une règle',
		'/rm-rule' => 'Supprime une règle',
	];

	$answer = "Les règles sont des regex PCRE, tu peux utiliser ce site pour les tester/bricoler : https://regex101.com/\n";
	$answer .= 'Commandes diponibles :';

	foreach ($cmds as $k => $v)
	{
		$answer .= sprintf( "\n\t%s\t%s", $k, $v);
	}

	return $answer;
}

function paginate_api( SimpleApiClient $api, string $endpoint, array $args)
{
	$data['answer'] = [];
	$next = null;

	while (true)
	{
		$args['cursor'] = $next ?? '';

		$r = $api->call( $endpoint, $args );

		$next = $r['response_metadata']['next_cursor'] ?? null;
		$next = empty($next) ? null : $next;

		if ( $r['ok'] === true && count($r) > 1 )
		{
			$tmp = array_keys($r);
			$data['answer'] = array_merge( $data['answer'], $r[$tmp[1]] );
		}
		else {
			return $r;
		}

		if ( !$next )
		{
			$r['answer'] = $data['answer'];
			return $r;
		}
	}
}