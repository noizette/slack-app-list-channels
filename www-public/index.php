<?php

require '../vendor/autoload.php';
require '../config.local.php';
require '../utils.php';

use Psr\Log\LogLevel;
use SlackPhp\BlockKit\Surfaces\AppHome;
use SlackPhp\Framework\{App, Context, StderrLogger};

App::new()
	->withLogger(new StderrLogger(LogLevel::DEBUG, LOG_FILE))
	
	->event('channel_created', function (Context $ctx)
	{
		$id = $ctx->payload()->get('event.channel.id');
		$user = $ctx->payload()->get('event.channel.creator');
		(new WebHook)->sendMessage( ANNOUNCE_CHAN, sprintf('<@%s> a créé un nouveau channel <#%s>', $user, $id ) );
		$ctx->api( 'conversations.join', [ 'channel' => $id ] );
	})
	->event('channel_rename', function (Context $ctx)
	{
		$id = $ctx->payload()->get('event.channel.id');
		(new WebHook)->sendMessage( ANNOUNCE_CHAN, sprintf('Un channel a changé de nom : <#%s>', $id ) );
		parallel_cache();
	})
	->event('channel_deleted', function (Context $ctx)
	{
		(new WebHook)->sendMessage( ANNOUNCE_CHAN, 'Un channel a été SUPPRIMÉ' );
		parallel_cache();
	})

	// Handles slash command to edit rules
	->command('help', function (Context $ctx)
	{
		$ctx->ack( get_help() );
	})
	// Send help on any message received
	// message.im subscription seems to send a lot of requests?
	->event( 'message', function (Context $ctx)
	{
		$id = $ctx->payload()->get('event.user');
		$ctx->api( 'chat.postMessage', [ 'channel' => $id, 'text' => get_help() ] );
	})
	->command('list', function (Context $ctx)
	{
		[ $titles, $rules ] = get_categories();

		$answer = "*Catégories*\n";
		foreach ($titles as $k => $v)
		{
			$answer .= sprintf( "\t'%s' => '%s'\n", $k, $v );
		}
		$answer .= "\n*Règles*\n";
		foreach ($rules as $k => $v)
		{
			$answer .= sprintf( "\t'%s' => '%s'\n", $k, $v );
		}
        $ctx->ack( $answer );
	})
	->command('add-rule', function (Context $ctx)
	{
		$args = $ctx->payload()->get('text');
		$args = explode( ' ', $args);

		if ( count($args) != 2 )
		{
			$ctx->ack( 'La commande prend 2 arguments : /add-rule <règle> <catégorie-id>' );
			return;
		}

		$answer = add_rule( $args[0], $args[1]) !== false ? 'Règle ajoutée (il peut y avoir un délai sur l\'affichage dû à la mise en cache)':'Erreur, règle pas ajoutée';

		$ctx->ack( $answer );
	})
	->command('rm-rule', function (Context $ctx)
	{
		$args = $ctx->payload()->get('text');
		$args = explode( ' ', $args);

		if ( count($args) != 1 )
		{
			$ctx->ack( 'La commande prend 1 argument : /rm-rule <règle>' );
			return;
		}

		$answer = rm_rule( $args[0]) !== false ? 'Règle supprimée (il peut y avoir un délai sur l\'affichage dû à la mise en cache)':'Erreur, règle pas supprimée';

		$ctx->ack( $answer );
	})
	->command('add-cat', function (Context $ctx)
	{
		$args = $ctx->payload()->get('text');
		$id = substr( $args, 0, strpos($args, ' '));
		$title = substr( $args, strlen($id) );

		if ( empty($id) && empty($title) )
		{
			$ctx->ack( 'La commande prend 2 arguments : /add-rule <règle> <catégorie-id>' );
			return;
		}

		$answer = add_cat( $id, $title) !== false ? 'Catégorie ajoutée':'Erreur, catégorie pas ajoutée';

		$ctx->ack( $answer );
	})
	->command('rm-cat', function (Context $ctx)
	{
		$args = $ctx->payload()->get('text');
		$args = explode( ' ', $args);

		if ( count($args) != 1 )
		{
			$ctx->ack( 'La commande prend 1 argument : /rm-rule <règle>' );
			return;
		}

		$answer = rm_cat( $args[0]) !== false ? 'Catégorie supprimée':'Erreur, catégorie pas supprimée';

		$ctx->ack( $answer );
	})

	// Handles when the app "home" is accessed.
	->event('app_home_opened', function (Context $ctx)
	{
		$date = new DateTime();
		$now = new DateTime();
		$user = $ctx->payload()->get('event.user');
		[ $cat_titles, $cat_rules ] = get_categories();

		$home = AppHome::new();
		$home->newSection()->mrkdwnText( MESSAGES['header'] );
		$home->divider();

		// Trigger cache creation if needed
		if ( !file_exists( CACHE_FILE ) )
		{
			parallel_cache();
			$home->newSection()->mrkdwnText( MESSAGES['caching'] );
			$ctx->appHome()->update($home);
			return;
		}

		// Load cache if exists + check TTL
		if ( ( $cache = file_get_contents( CACHE_FILE ) ) !== false )
		{
			$cache = json_decode( $cache, true );
			$channels = $cache['channels'];

			if ( isset( $cache['meta']['date']) && $cache['meta']['date'] < ( time() - CACHE_DELAY ) )
			{
				parallel_cache();
			}
		}

		foreach ($channels as $chan)
		{
			$is_member =  in_array( $user, $chan['members'] ) ? JOINED_CHAN : NOT_JOINED_CHAN;

			$categories[ $chan['cat'] ][] = array(
				'id'			=> $chan['id'],
				'name'			=> $chan['name'],
				'num_members'	=> $chan['num_members'],
				'topic'			=> $chan['topic'],
				'cat'			=> $chan['cat'],
				'is_member'		=> $is_member,
				'latest'		=> $chan['latest'],
			);
		}

		// Sort categories by title and put "Other" cat in last position
		asort($cat_titles, SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL );
		$other = $cat_titles['other'];
		unset( $cat_titles['other'] );
		$cat_titles['other'] = $other;

		// Sort and display categories
		foreach ($cat_titles as $k => $title)
		{
			$text = '';
			$home->newSection()->mrkdwnText( sprintf( "\n*%s*", $title) );
			$home->divider();

			$cat = $categories[$k];

			usort( $cat, function( $a, $b){
				return $a['latest'] < $b['latest'];
			});

			foreach ( $cat as $chan)
			{
				$date->setTimestamp( $chan['latest'] );
				$diff = $now->diff($date);

				if ($diff->y !== 0) {
					$ago = sprintf('il y a %d ans', $diff->y );
				} elseif ($diff->m !== 0) {
					$ago = sprintf('il y a %d mois', $diff->m );
				} elseif ($diff->d == 0) {
					$ago = 'aujourd\'hui';
				} elseif ($diff->d == 1) {
					$ago = 'hier';
				} elseif ($diff->d <= 7) {
					$ago = sprintf('il y a %d jours', $diff->d );
				} elseif ($diff->d <= 31) {
					$ago = sprintf('il y a %d semaines', $diff->d / 7 );
				} else {
					$ago = $date->format('d/m/Y');
				}

				$text .= sprintf( "%s <#%s>\t%s\t\t_%s membres_, %s\n\n", $chan['is_member'], $chan['id'], $chan['topic'], $chan['num_members'], $ago );
			}

			$home->newSection()->mrkdwnText( $text );
		}

		// Benchmark
		// $time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
		// $home->newSection()->mrkdwnText( 'Exec time: '. $time );

		// Update home page
		$ctx->appHome()->update($home);
	})

	// Run that app to process the incoming Slack request.
	->run();
