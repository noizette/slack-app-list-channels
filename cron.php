<?php

require 'vendor/autoload.php';
require 'config.local.php';
require 'utils.php';

use SlackPhp\Framework\AppConfig;
use SlackPhp\Framework\Clients\SimpleApiClient;

$appConfig = new AppConfig();
$tokenStore = $appConfig->getTokenStore();
$api = new SimpleApiClient( $tokenStore->get( null, null) );

$bot = $api->call( 'auth.test' , [] );
$bot = $bot['user_id'];
$to_cache = array();
[ $cat_titles, $cat_filters ] = get_categories();


$chans = paginate_api( $api, 'conversations.list', [ 'exclude_archived' => true, 'limit' => 100 ] );
$chans['channels'] = $chans['answer'] ?? array();

foreach ($chans['channels'] as $chan)
{
	// If bot is not yet member of chan, it won't be able to get last_read/last msg
	if ( $chan['is_member'] === false )
	{
		$api->call( 'conversations.join', [ 'channel' => $chan['id'] ] );
	}

	$latest = 0;
	$history = $api->call( 'conversations.history', [ 'channel' => $chan['id'], 'limit' => 1 ] );
	if ( $history['ok'] === true && !empty($history['messages']))
	{
		$msg = $history['messages'][0];

		// If last chan msg is from bot (ie. it's the join message)
		// Use last_read ts instead (except for announche chan, the only chan where bot posts)
		if ( $chan['id'] !== ANNOUNCE_CHAN_ID && $msg['user'] === $bot )
		{
			$info = $api->call( 'conversations.info', [ 'channel' => $chan['id'] ] );
			if ( $info['ok'] === true )
			{
				$latest = (int) $info['channel']['last_read'];
			}
		}
		else
		{
			$latest = (int) $msg['ts'];
			
			try {
				// Only check last message for replies, so if the recentest message is a reply to another thread, we won't get it
				$reply = $api->call( 'conversations.reply', [ 'channel' => $chan['id'], 'ts' => $latest, 'limit' => 1 ] );
				$latest = $reply['messages'][0]['ts'];
			} catch ( Exception $e ) {
				// osef
			}

		}
	}
	unset( $history, $info, $msg);

	$members = paginate_api( $api, 'conversations.members', [ 'channel' => $chan['id'], 'limit' => 100 ] );  
	$members = $members['answer'] ?? array();

	$newchan = array(
		'id'			=> $chan['id'],
		'name'			=> $chan['name'],
		'num_members'	=> $chan['num_members'],
		'topic'			=> $chan['topic']['value'],
		'members'		=> $members,
		'latest'		=> $latest,
	);


	// Determine category
	foreach ( $cat_filters as $regex => $cat)
	{
		if ( preg_match( "#$regex#", $chan['name'] ) )
		{
			$newchan['cat'] = $cat;
			break;
		}
	}

	if ( !isset( $newchan['cat'] ) )
	{
		$newchan['cat'] = 'other';
	}

	$to_cache[ $newchan['id'] ] = $newchan;
}

// Write cache
if ( !empty( $to_cache) )
{
	$data['meta']       = [ 'date' => time() ];
	$data['channels']   = $to_cache;
	$data = json_encode( $data );
	$path = dirname( CACHE_FILE );
	if ( is_writable( CACHE_FILE ) || is_writable( $path ) )
	{
		if ( !file_put_contents( CACHE_FILE, $data) )
		{
			printf( '%s, l.%s: Failed to write cache', __FILE__, __LINE__);
		}
	} else {
		printf( '%s, l.%s: Cache file/path is not writable', __FILE__, __LINE__);
	}
}